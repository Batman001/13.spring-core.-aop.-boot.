package org.shop.config.java;

import java.util.HashMap;
import java.util.Map;

import org.shop.DataInitializer;
import org.shop.ProductInitializer;
import org.shop.ProposalInitializer;
import org.shop.SellerInitializer;
import org.shop.UserInitializer;
import org.shop.api.ProductService;
import org.shop.api.UserService;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataInitializresConfig {

  @Autowired
  private ProductService productService;

  @Autowired
  private UserService userService;

  @Bean
  public ProductInitializer productInitializer() {
    return new ProductInitializer(productService);
  }

  @Bean(autowire = Autowire.BY_NAME)
  public ProposalInitializer proposalInitializer() {
    return new ProposalInitializer();
  }

  @Bean
  public SellerInitializer sellerInitializer() {
    return new SellerInitializer();
  }

  @Bean
  public UserInitializer userInitializer() {
    return new UserInitializer(userService);
  }

  @Bean
  public Map<Long, String> sellerNames() {
    Map<Long, String> sellerNames = new HashMap<>();
    sellerNames.put(1L, "Kirill_1");
    sellerNames.put(2L, "Kirill_2");
    sellerNames.put(32L, "Kirill_3");
    sellerNames.put(321L, "Kirill_4");
    return sellerNames;
  }

  @Bean(initMethod = "initData")
  public DataInitializer dataInitializer() {
    return new DataInitializer();
  }
}
