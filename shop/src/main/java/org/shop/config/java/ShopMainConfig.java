package org.shop.config.java;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration

@Import({RepositoryConfig.class, ServiceConfig.class, DataInitializresConfig.class, FactoryConfig.class})
public class ShopMainConfig {

}
