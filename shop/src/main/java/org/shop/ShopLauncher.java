package org.shop;

import org.shop.api.ItemService;
import org.shop.api.OrderService;
import org.shop.api.ProductService;
import org.shop.api.ProposalService;
import org.shop.api.SellerService;
import org.shop.api.UserService;
import org.shop.config.java.ShopMainConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.ejb.config.JeeNamespaceHandler;

/**
 * The ShopLauncher class.
 */
public class ShopLauncher {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext();
		((AnnotationConfigApplicationContext) ctx).register(ShopMainConfig.class);
		((ConfigurableApplicationContext) ctx).refresh();
		
		UserService userService = (UserService) ctx.getBean("userService");
		ItemService itemService = (ItemService) ctx.getBean("itemService");
		OrderService orderService = (OrderService) ctx.getBean("orderService");
		ProductService productService = (ProductService) ctx.getBean("productService");
		ProposalService proposalService = (ProposalService) ctx.getBean("proposalService");
		SellerService sellerService = (SellerService) ctx.getBean("sellerService");

		System.out.println("������������");
		userService.getUsers().stream().forEach(user -> System.out.println(user));
		System.out.println("��������");
		sellerService.getSellers().stream().forEach(seller -> System.out.println(seller));
		System.out.println("��������");
		productService.getProducts().stream().forEach(product -> System.out.println(product));
		System.out.println("�� ID");
		System.out.println(proposalService.getProposalsByProductId(1l));
		
		((ConfigurableApplicationContext) ctx).close();

	}
}
